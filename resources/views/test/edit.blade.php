<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Edit test') }}
        </h2>
    </x-slot>

    <div class="row container">
        <div class="col-md-12">
            <form class="row" action="/test/{{$test->id}}" method="POST">
                <!-- Token -->
                @csrf
                <!-- Especificar metodo PUT -->
                @method('PUT')
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_test">Name</label>
                        <input type="text" class="form-control" id="name_test" name="name_test" aria-describedby="emailHelp" value="{{$test->name_test}}">
                    </div>
                    <div class="row mt-5">
                        <div class="col-9">
                            <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>
                        <div class="col-3">
                            <a href="/test" class="btn btn-outline-secondary btn-block">Cancel</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="desc_test">Description</label>
                        <textarea class="form-control" id="desc_test" name="desc_test" rows="5">{{$test->desc_test}}</textarea>
                    </div>
                </div>

            </form>
        </div>
    </div>
</x-app-layout>