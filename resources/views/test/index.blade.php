<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Showing all tests') }}
        </h2>
    </x-slot>

    @if( isset($tests['no_questions']) )
    <span class="h2">No hay preguntas aún que mostrar. ¡Agrega la primer pregunta hora!</span>
    @else

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name Test</th>
                        <th scope="col">Description</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tests as $test)
                    <tr>
                        <td>{{ $test->id }}</td>
                        <td>{{ $test->name_test }}</td>
                        <td>{{ $test->desc_test }}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="/test/{{$test->id}}/show" class="btn btn-sm btn-info btn-block">Show Test</a>
                                </div>
                                <div class="col-md-3">
                                    <a href="/test/{{$test->id}}/showquestion" class="btn btn-sm btn-info btn-block">Show Questions</a>
                                </div>
                                <div class="col-md-2">
                                    <a href="/test/{{$test->id}}/addquestion" class="btn btn-sm btn-primary btn-block">Add Question</a>
                                </div>
                                <div class="col-md-2">
                                    <a href="/test/{{$test->id}}/edit" class="btn btn-sm btn-warning btn-block">Edit Test</a>
                                </div>
                                <div class="col-md-2">
                                    <form action="{{ route('test.destroy', $test->id) }}" method="POST">
                                        <!-- Token -->
                                        @csrf
                                        <!-- Especificar metodo PUT -->
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger btn-block">Delete Test</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</x-app-layout>