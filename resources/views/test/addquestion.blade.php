<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Add new question') }}
        </h2>
    </x-slot>

    <div class="row container">
        <div class="col-md-12">
            <form action="/question" method="POST">
                @csrf
                <input type="text" value="{{ $id }}" hidden name="id_test" id="id_test">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="question">Question </label>
                            <input type="text" class="form-control" id="question" name="question" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="question">Can the user answer anything? </label>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="isOpen" name="isOpen">
                            <label class="form-check-label" for="isOpen">Free Answer</label>
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-9">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                    </div>
                    <div class="col-3">
                        <a href="/test" class="btn btn-outline-secondary btn-block">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>