<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Showing an specific test') }}
        </h2>
    </x-slot>

    <div class="row container">
        <div class="col-md-12">
            <span class="h1">{{ $test['test_data']->name_test }}</span>
        </div>
        <div class="col-md-12">
            @php $i = 0; @endphp
            <div class="row">
                @foreach ( $test['questions_data'] as $question)
                @php $i++; @endphp

                @if ( $question->isOpen == 1 )
                <div class="col-md-6">
                    <div class="form-group">
                        @if( $i != 1 || $i != 2 )
                        <hr class="my-4">
                        @endif
                        <b><label for="q_{{ $question->id }}">{{ $i }}.- {{ $question->question }}</label></b>
                        <input type="text" class="form-control" id="t-{{$test['test_data']->id}}_q-{{ $question->id }}_a-0" required>
                    </div>
                </div>
                @else
                <div class="col-md-6">
                    <div class="form-group">
                        <b><label for="q_{{ $question->id }}">{{ $i }}.- {{ $question->question }} *</label></b>
                        @foreach ( $test['answers_data'][$question->id] as $answer)
                        @if ( true )
                        <div class="custom-control custom-radio">
                            <input type="radio" id="t-{{$test['test_data']->id}}_q-{{ $question->id }}_a-{{ $answer->id }}" name="t-{{$test['test_data']->id}}_q-{{ $question->id }}" class="custom-control-input">
                            <label class="custom-control-label" for="t-{{$test['test_data']->id}}_q-{{ $question->id }}_a-{{ $answer->id }}">{{ $answer->answer }}</label>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
                @endif
                @endforeach
                <div class="col-12"><hr></div>
                <div class="col-md-4 offset-8">
                    <button class="btn btn-block btn-success">Send</button>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>