<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Create new test') }}
        </h2>
    </x-slot>

    <div class="row container">
        <div class="col-md-12">
            <form class="row" action="/test" method="POST">
                @csrf
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_test">Name</label>
                        <input type="text" class="form-control" id="name_test" name="name_test" required>
                    </div>
                    <div class="row mt-5">
                        <div class="col-9">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </div>
                        <div class="col-3">
                            <a href="/dashboard" class="btn btn-outline-secondary btn-block">Cancel</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="desc_test">Description</label>
                        <textarea class="form-control" id="desc_test" name="desc_test" rows="5" required></textarea>
                    </div>
                </div>

            </form>
        </div>
    </div>
</x-app-layout>