<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Add new answer') }}
        </h2>
    </x-slot>

    <div class="row container">
        <div class="col-md-12">
            <form action="/answer" method="POST">
                @csrf
                <input type="text" value="{{ $id }}" hidden name="id_question" id="id_question">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="answer">Answer Text </label>
                            <input type="text" class="form-control" id="answer" name="answer" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="isCorrect">Is it Correct? </label>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="isCorrect" name="isCorrect">
                            <label class="form-check-label" for="isCorrect">Yes, it is</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <small class="text-muted">If this answer cannot be answered as correct or incorrect, check the box to indicate that whatever you answer is correct, so for all choices. Remember that this answer will be show as option to choose.</small>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-9">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                    </div>
                    <div class="col-3">
                        <a href="/question" class="btn btn-outline-secondary btn-block">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>