<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
             
            @if ( count($questions) > 0 )
            {{ __('Showing all questions from') }} {{ $questions[0]->name_test }}
            @else
            {{ __('Nothing to show at this time') }}
            @endif
        </h2>
    </x-slot>

    @if ( count($questions) <= 0 )
        <span class="h2">No hay preguntas aún en este test.</span>
    @else
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name Test</th>
                        <th scope="col">Question</th>
                        <th scope="col">Is it Free Answer?</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($questions as $question)
                    <tr>
                        <td>{{ $question->id }}</td>
                        <td>{{ $question->name_test }}</td>
                        <td>{{ $question->question }}</td>
                        <td>{{ ($question->isOpen == 1) ? 'Yes':'No' }}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="/question/{{$question->id}}/showanswers" class="btn btn-xs btn-info btn-block">Show Answers</a>
                                </div>
                                @if( $question->isOpen != 1 )
                                <div class="col-md-3">
                                    <a href="/question/{{$question->id}}/addanswer" class="btn btn-xs btn-primary btn-block">Add Answer</a>
                                </div>
                                @endif
                                <div class="col-md-3">
                                    <a href="/question/{{$question->id}}/edit" class="btn btn-xs btn-warning btn-block">Edit</a>
                                </div>
                                <div class="col-md-3">
                                    <form action="{{ route('question.destroy', $question->id) }}" method="POST">
                                        <!-- Token -->
                                        @csrf
                                        <!-- Especificar metodo PUT -->
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-xs btn-danger btn-block">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
</x-app-layout>