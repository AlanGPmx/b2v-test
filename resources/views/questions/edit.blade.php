<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Edit a question') }}
        </h2>
    </x-slot>

    <div class="row container">
        <div class="col-md-12">
            <form action="/question/{{$questions['selected']->id}}" method="POST">
                <!-- Token -->
                @csrf
                <!-- Especificar metodo PUT -->
                @method('PUT')
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="question">Question </label>
                            <input type="text" class="form-control" id="question" name="question" value="{{$questions['selected']->question}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="question">Choose a test </label>
                        <select class="custom-select" name="id_test">
                            <option disabled selected>Choose a test</option>
                            @foreach ($questions['list'] as $test)
                            @if($questions['selected']->id_test == $test->id)
                            <option selected value="{{ $test->id }}">{{ $test->name_test }}</option>
                            @else
                            <option value="{{ $test->id }}">{{ $test->name_test }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="question">Can the user answer anything? </label>
                        <div class="form-group form-check">
                            @if($questions['selected']->isOpen == '1')
                            <input type="checkbox" checked="true" class="form-check-input" id="isOpen" name="isOpen">
                            @else
                            <input type="checkbox" class="form-check-input" id="isOpen" name="isOpen">
                            @endif
                            <label class="form-check-label" for="isOpen">Yes, it's an open question</label>
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-9">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                    </div>
                    <div class="col-3">
                        <a href="/test" class="btn btn-outline-secondary btn-block">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>