<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Showing all answers from') }}
        </h2>
    </x-slot>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Question to which it belongs</th>
                        <th scope="col">Answer Text</th>
                        <th scope="col">Is it Correct?</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($answers as $answer)
                    <tr>
                        <td>{{ $answer->id }}</td>
                        <td>{{ $answer->question }}</td>
                        <td>{{ $answer->answer }}</td>
                        <td>{{ ($answer->isCorrect == null) ? 'No':'Yes' }}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="/answer/{{$answer->id}}/edit" class="btn btn-xs btn-warning btn-block">Edit</a>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{ route('answer.destroy', $answer->id) }}" method="POST">
                                        <!-- Token -->
                                        @csrf
                                        <!-- Especificar metodo PUT -->
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-xs btn-danger btn-block">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>