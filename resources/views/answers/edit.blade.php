<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Edit an answer') }}
        </h2>
    </x-slot>

    <div class="row container">
        <div class="col-md-12">
            <form action="/answer/{{$answers['selected']->id}}" method="POST">
                <!-- Token -->
                @csrf
                <!-- Especificar metodo PUT -->
                @method('PUT')
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="answer">Answer </label>
                            <input type="text" class="form-control" id="answer" name="answer" value="{{$answers['selected']->answer}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="question">Choose a question </label>
                        <select class="custom-select" name="id_question">
                            <option disabled selected>Choose a test</option>
                            @foreach ($answers['list'] as $answer)
                            @if($answers['selected']->id_question == $answer->id)
                            <option selected value="{{ $answer->id }}">{{ $answer->question }}</option>
                            @else
                            <option value="{{ $answer->id }}">{{ $answer->question }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="question">Can the user answer anything? </label>
                        <div class="form-group form-check">
                            @if($answers['selected']->isCorrect == '1')
                            <input type="checkbox" checked="true" class="form-check-input" id="isCorrect" name="isCorrect">
                            @else
                            <input type="checkbox" class="form-check-input" id="isCorrect" name="isCorrect">
                            @endif
                            <label class="form-check-label" for="isCorrect">Yes, it's an open question</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <small class="text-muted">If this answer cannot be answered as correct or incorrect, check the box to indicate that whatever you answer is correct, so for all choices. Remember that this answer will be show as option to choose.</small>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-9">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                    </div>
                    <div class="col-3">
                        <a href="/test" class="btn btn-outline-secondary btn-block">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>