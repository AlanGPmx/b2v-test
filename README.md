<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Requirements

• Instalación de Laravel 7.x u 8.x<br>
• Instalar sistema auth de Laravel<br>
• Añadir campo 'last_name' después de campo 'name' en la migración 'users'<br>
• Controlador de 'test'<br>
• Migraciones de 'test'<br>
• Modelo(s)<br>
• Vista(s)<br>
• Que al iniciar sesión redirija a ver el test<br>
• Que pueda responder el test [_opcional_]<br>

## Live demostration

[Live Demostration](https://fierce-cove-23834.herokuapp.com)

## About

This technical test was developed by Alan Garduño for B2B-SF. 2021.
