<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return redirect('test');
})->name('dashboard');


/**
 * Rutas correspondientes a las respuestas
 */
// Vista para mostrar las respuestas de una pregunta mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->get('/question/{id}/showanswers', 'App\Http\Controllers\QuestionController@showanswers');

// Vista para agregar respuestas a una pregunta mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->get('/question/{id}/addanswer', 'App\Http\Controllers\QuestionController@addanswer');

// Vista para editar una respuesta mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->resource('/answer/edit', 'App\Http\Controllers\AnswerController@edit');

// Vista para agregar respuestas al test mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->resource('/answer', 'App\Http\Controllers\AnswerController');

/**
 * Rutas correspondientes a las Preguntas
 */
// Vista para mostrar las preguntas de un test mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->get('/test/{id}/showquestion', 'App\Http\Controllers\TestController@showquestion');

// Vista para agregar preguntas al test mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->get('/test/{id}/addquestion', 'App\Http\Controllers\TestController@addquestion');

// Vista para editar una pregunta mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->resource('/question/edit', 'App\Http\Controllers\QuestionController@edit');

// Vista para agregar preguntas al test mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->resource('/question', 'App\Http\Controllers\QuestionController');

/**
 * Rutas correspondientes a los tests
 */

// Vista para crear un nuevo test mediante el controlador
Route::get('/test/{id}/show', 'App\Http\Controllers\TestController@show');

// Vista para crear un nuevo test mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->resource('/test/create', 'App\Http\Controllers\TestController@create');

// Vista para editar un test mediante el controlador
Route::middleware(['auth:sanctum', 'verified'])->resource('/test/edit', 'App\Http\Controllers\TestController@edit');

// Vista para crear un nuevo test mediante su controlador
Route::middleware(['auth:sanctum', 'verified'])->resource('/test', 'App\Http\Controllers\TestController');
