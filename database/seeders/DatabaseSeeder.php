<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(2)->create();
        \App\Models\Test::create( array('name_test' => 'Prueba de Matemáticas', 'desc_test' => 'Nivel: Facil') );
        \App\Models\Test::create( array('name_test' => 'Prueba de Español', 'desc_test' => 'Nivel: Intermedio') );
        \App\Models\Test::create( array('name_test' => 'English Test', 'desc_test' => 'Level: Easy') );
        \App\Models\Test::create( array('name_test' => 'Test Vocacional', 'desc_test' => 'Apoyo para alumnos de secundaria') );
    }
}
