<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Test;
use App\Models\Question;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allTest = Test::all(); // Se manda a llamar todos los campos de la tabla 'tests'
        return view('test.index')->with('tests', $allTest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('test.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = new Test();
        $test->name_test = $request->get('name_test');
        $test->desc_test = $request->get('desc_test');
        $test->save();

        return redirect('/test');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Se manda a llamar los campos de la tabla 'tests' buscando por su ID
        $test['test_data'] = Test::find($id);

        // Se seleccionan todas las preguntas que pertenecen a este test
        $test['questions_data'] = DB::select('SELECT * FROM questions WHERE id_test = :id_test', ['id_test' => $id]);

        foreach ($test['questions_data'] as $question) {
            if ($question->isOpen == 0) {
                // Se seleccionan todas las respuestas que pertenecen a esta pregunta
                $test['answers_data'][$question->id] = DB::select('SELECT * FROM answers WHERE id_question = :id_question', ['id_question' => $question->id]);
            }
        }

        if( count($test['questions_data']) <= 0 ){
            $test['no_questions'] = true;
            return view('test.index')->with('tests', $test);
        }else{
            return view('test.show')->with('test', $test);
        }
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = Test::find($id);
        return view('test.edit')->with('test', $test);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test = Test::find($id);
        $test->name_test = $request->get('name_test');
        $test->desc_test = $request->get('desc_test');
        $test->save();

        return redirect('/test');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $test = Test::find($id);
        $test->delete();

        return redirect('/test');
    }

    /**
     * Agrega una pregunta al test
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addquestion($id = 'none')
    {
        return view('test.addquestion')->with('id', $id);
    }

    /**
     * Muestra las preguntas de un test
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showquestion($id = 'none')
    {
        $allQuestions = Question::join('tests', 'tests.id', '=', 'questions.id_test')->where('tests.id', $id)->get(['questions.*', 'tests.name_test']); // Se manda a llamar todos los campos de la tabla 'question'
        return view('questions.index')->with('questions', $allQuestions);
    }
}
