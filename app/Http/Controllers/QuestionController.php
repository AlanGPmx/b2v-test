<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Test;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allQuestions = Question::join('tests', 'tests.id', '=', 'questions.id_test')->get(['questions.*', 'tests.name_test']); // Se manda a llamar todos los campos de la tabla 'question'
        
        if( count($allQuestions) <= 0 ){
            $allQuestions['no_questions'] = true;
            return view('questions.index')->with('questions', $allQuestions);
        }else{
            return view('questions.index')->with('questions', $allQuestions);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questions.create')->with('tests', Test::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = new Question();
        $question->id_test = $request->get('id_test');
        $question->question = $request->get('question');
        $question->isOpen = ($request->get('isOpen') == 'on') ? true : false;
        $question->save();

        return redirect('/test');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question['selected'] = Question::find($id);
        $question['list'] = Test::all();
        return view('questions.edit')->with('questions', $question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $question = Question::find($id);
        $question->id_test = $request->get('id_test');
        $question->question = $request->get('question');
        $question->isOpen = ($request->get('isOpen') == 'on') ? true : false;
        $question->save();

        return redirect('/test');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->delete();

        return redirect('/test');
    }

    /**
     * Agrega una respuesta a la pregunta
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addanswer($id = 'none')
    {
        return view('questions.addanswer')->with('id', $id);
    }

    /**
     * Muestra las respuestas de una pregunta
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showanswers($id = 'none')
    {
        $allAnswers = Answer::join('questions', 'questions.id', '=', 'answers.id_question')->where('questions.id', $id)->get(['answers.*', 'questions.question']); // Se manda a llamar todos los campos de la tabla 'question'
        return view('answers.index')->with('answers', $allAnswers);
    }
}
