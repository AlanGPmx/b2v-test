<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Answer;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allAnswers = Answer::join('questions', 'questions.id', '=', 'answers.id_question')->get(['answers.*', 'questions.question']); // Se manda a llamar todos los campos de la tabla 'question'
        return view('answers.index')->with('answers', $allAnswers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('answers.create')->with('questions', Question::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $answer = new Answer();
        $answer->id_question = $request->get('id_question');
        $answer->answer = $request->get('answer');
        $answer->isCorrect = ($request->get('isCorrect') == 'on') ? true : false;
        $answer->save();

        return redirect('/test');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $answer['selected'] = Answer::find($id);
        $answer['list'] = Question::all();
        return view('answers.edit')->with('answers', $answer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::find($id);
        $answer->id_question = $request->get('id_question');
        $answer->answer = $request->get('answer');
        $answer->isCorrect = ($request->get('isCorrect') == 'on') ? true : false;
        $answer->save();

        return redirect('/test');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer = Answer::find($id);
        $answer->delete();

        return redirect('/test');
    }
}
